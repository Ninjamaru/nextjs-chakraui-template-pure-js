import { Box, Container, Stack, Heading } from '@chakra-ui/react'
import React from 'react'
import Layout from '../components/layout'

const Home = () => {
  const sampleWidth = 200;
  return (
    <Layout>
      <Box>

        <Container maxW={'3xl'}>
          <Stack
            as={Box}
            textAlign={'center'}
            spacing={{ base: 8, md: 14 }}
            py={{ base: 20, md: 36 }}>

            <Heading
              fontWeight={600}
              fontSize={{ base: '2xl', sm: '4xl', md: '6xl' }}
              lineHeight={'110%'}>
              Hello! <br />
            </Heading>
          </Stack>

        </Container>


      </Box>
    </Layout>
  )
}

export default Home